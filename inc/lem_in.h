/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 17:40:09 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:34:26 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "../libft/libft.h"
# include <stdlib.h>

typedef struct		s_room
{
	char			*name;
	char			*x;
	char			*y;
	unsigned char	visited:1;
	int				level;
	t_list			*links;
	int				way_id;
	unsigned char	occupied:1;
}					t_room;

typedef struct		s_ant
{
	int				ant_num;
	unsigned char	finished:1;
	t_dllist		*curr_way_item;
}					t_ant;

typedef struct		s_flags
{
	unsigned char	error:1;
	unsigned char	error_detalization:1;
	unsigned char	print_help:1;
	unsigned char	print_ways:1;
	unsigned char	show_ant_way:1;
	int				ant_num_to_show;
	t_dllist		*ant_way;
}					t_flags;

typedef struct		s_ant_farm
{
	t_flags			flags;
	t_list			*lines;
	int				ant_count;
	int				room_count;
	t_dllist		*rooms;
	t_dllist		*last;
	t_dllist		*start;
	t_dllist		*end;
	unsigned char	error:1;
	t_dllist		*ways;
	int				the_shortest_way_len;
	t_dllist		*ants;
}					t_ant_farm;

typedef struct		s_queue
{
	t_dllist		*add_here;
	t_dllist		*get_here;
}					t_queue;

void				create_farm(t_ant_farm *farm);
void				save_line(char *line, t_ant_farm *farm);
void				verify_parse(char **splitted, t_ant_farm *farm,
									char *state, char *line);
void				command(char *command, t_ant_farm *farm, char *state,
									int *line_num);
void				validation_ant_count(char **splitted, char *state,
									char *line);
void				validation_room(char **splitted, char *state, char *line);
void				room(char **splitted, t_ant_farm *farm, char *state,
									char *line);
void				validation_edge(char **splitted, char *state, char *line);
void				parse_ant_count(char **splitted, t_ant_farm *farm,
									char *state);
void				parse_room(char **splitted, t_ant_farm *farm, char *state);
void				parse_edge(char **splitted, t_ant_farm *farm, char *state);
void				check_end_reachability(t_ant_farm *farm, char *state);
void				analyze_graph(t_ant_farm *farm);
t_list				*set_rooms_depth_dijkstra(t_ant_farm *farm);
void				add_link_to_queue(t_queue *queue, t_list *link);
void				add_links_to_queue(t_queue *queue, t_list *link);
t_list				*get_link_from_queue(t_queue *queue);
void				set_rooms_links_as_unused(t_ant_farm *farm);
void				set_visited_rooms_as_unvisited(t_ant_farm *farm);
void				find_ways(t_ant_farm *farm);
t_list				*get_link_lowest_level_unvisited(t_list *links);
t_room				*get_fake_end_room(t_ant_farm *farm, int way_id);
void				ants_on_the_way(t_ant_farm *farm);
void				one_move(t_ant_farm *farm);
void				put_ants_in_rooms(t_ant_farm *farm);
void				print_ants_on_the_way(t_ant_farm *farm);
void				free_ways(t_ant_farm *farm);
void				free_way(t_dllist *way);
void				create_ant_list(t_ant_farm *farm);
int					does_all_ants_finished(t_ant_farm *farm);
void				sort_ways(t_ant_farm *farm);
void				print_ants_in_end_in_one_way(t_ant_farm *farm);
void				calc_ways_length(t_ant_farm *farm);
void				error_processor(t_flags *flags, char state, int *line_num);
void				print_lines_of_file(t_list *lines);
void				farm_free(t_ant_farm *farm);
int					process_flag(char *flag, t_flags *flags);
void				usage(void);
void				options_processor(t_ant_farm *farm);
void				print_ways(t_ant_farm *farm);

#endif
