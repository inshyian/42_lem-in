/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_edge.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 11:51:50 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 16:03:27 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static int		is_linked_rooms(t_room *room, t_room *add_in_room)
{
	t_list		*links;

	links = room->links;
	while (links)
	{
		if (!ft_strcmp(((t_room *)links->content)->name, add_in_room->name))
		{
			return (1);
		}
		links = links->next;
	}
	return (0);
}

static void		add_link_to_room(t_room *room, t_room *add_in_room)
{
	t_list		*link;

	link = ft_smalloc(sizeof(t_list));
	ft_bzero(link, sizeof(t_list));
	link->content = add_in_room;
	ft_lstaddend(&room->links, link);
}

static void		link_rooms(t_room *a, t_room *b)
{
	if (!is_linked_rooms(a, b) && !is_linked_rooms(b, a))
	{
		add_link_to_room(a, b);
		add_link_to_room(b, a);
	}
}

static t_room	*find_room_by_name(char *name, t_dllist *rooms)
{
	while (rooms)
	{
		if (!ft_strcmp(((t_room *)rooms->content)->name, name))
			return ((t_room *)rooms->content);
		rooms = rooms->next;
	}
	return (NULL);
}

void			parse_edge(char **splitted, t_ant_farm *farm, char *state)
{
	t_room		*a;
	t_room		*b;

	if (!ft_strcmp(splitted[0], splitted[1]))
		*state = -15;
	else
	{
		a = find_room_by_name(splitted[0], farm->rooms);
		b = find_room_by_name(splitted[1], farm->rooms);
		if (!a || !b)
			*state = -16;
		else
			link_rooms(a, b);
	}
}
