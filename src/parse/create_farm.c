/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_farm.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 17:16:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 11:40:14 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../inc/lem_in.h"

void				checks(t_ant_farm *farm, char *state, int *line_num)
{
	if (*line_num == 0)
		*state = -30;
	else if (*state == 1)
		*state = -35;
	else if (!farm->rooms && *state > 0)
		*state = -33;
	else if (!farm->start && *state > 0)
		*state = -31;
	else if (!farm->end && *state > 0)
		*state = -32;
	else if (*state == 2)
		*state = -34;
	if (*state < 0)
	{
		error_processor(&farm->flags, *state, line_num);
		farm->error = 1;
	}
}

static void			read(t_ant_farm *farm, char *state, int *line_num)
{
	char			*line;

	line = NULL;
	while (*state > 0 && get_next_line(0, &line) == 1)
	{
		save_line(line, farm);
		if (line[0])
			(*line_num)++;
		if (line[0] && line[0] != '#' && *state != 3)
			verify_parse(ft_strsplit(line, ' '), farm, state, line);
		else if (line[0] && line[0] != '#')
			verify_parse(ft_strsplit(line, '-'), farm, state, line);
		else if (line[0] && line[0] == '#' && line[1] == '#')
			command(line, farm, state, line_num);
		else if (line[0] == '\0')
		{
			ft_memdel((void **)&line);
			break ;
		}
		ft_memdel((void **)&line);
	}
	ft_memdel((void **)&line);
}

void				create_farm(t_ant_farm *farm)
{
	char			state;
	int				line_num;

	state = 1;
	line_num = 0;
	read(farm, &state, &line_num);
	checks(farm, &state, &line_num);
	if (state < 0)
		ft_lstdel(&farm->lines, &ft_memsdel);
}
