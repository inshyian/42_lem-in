/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation_edge.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 17:16:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:21:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void				validation_edge(char **splitted, char *state, char *line)
{
	if (ft_strsplits_count(splitted) == 2)
	{
		if (ft_strchrcount(line, '-') != 1)
			*state = -8;
		else if (splitted[0][0] == 'L' || ft_strchr(splitted[0], '-'))
			*state = -9;
		else if (splitted[1][0] == 'L' || ft_strchr(splitted[1], '-'))
			*state = -9;
	}
	else
		*state = -8;
}
