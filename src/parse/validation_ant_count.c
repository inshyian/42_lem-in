/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation_ant_count.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 17:16:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:20:19 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void				validation_ant_count(char **splitted, char *state,
								char *line)
{
	if (ft_strsplits_count(splitted) == 1)
	{
		if (ft_strchrcount(line, ' ') != 0)
			*state = -7;
		else if (!ft_isnum(splitted[0]))
			*state = -1;
		else if (ft_longexprcmp(splitted[0], '>', "2147483647"))
			*state = -2;
		else if (ft_longexprcmp(splitted[0], '<', "1"))
			*state = -3;
	}
	else
		*state = -7;
}
