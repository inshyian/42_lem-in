/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 14:36:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 11:57:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../inc/lem_in.h"

static int		is_valid_command(char *command)
{
	if (!ft_strcmp("##start", command))
		return (1);
	else if (!ft_strcmp("##end", command))
		return (1);
	return (0);
}

static void		do_command(char *command, t_ant_farm *farm, char *state)
{
	if (ft_strstr("##start", command))
	{
		if (farm->start)
			*state = -10;
		else
			farm->start = farm->last;
	}
	else if (ft_strstr("##end", command))
	{
		if (farm->end)
			*state = -11;
		else
			farm->end = farm->last;
	}
}

void			command(char *command, t_ant_farm *farm, char *state,
						int *line_num)
{
	char			*line;

	if (is_valid_command(command) && *state != 2)
		*state = -14;
	else if (is_valid_command(command) && *state == 2)
	{
		(*line_num)++;
		get_next_line(0, &line);
		save_line(line, farm);
		room(ft_strsplit(line, ' '), farm, state, line);
		if (*state == 2)
			do_command(command, farm, state);
		free(line);
	}
}
