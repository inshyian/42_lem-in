/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 21:27:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/08/24 21:40:02 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void		save_line(char *line, t_ant_farm *farm)
{
	t_list		*new;

	new = ft_lstnew(line, ft_strlen(line) + 1);
	ft_lstaddend(&farm->lines, new);
}
