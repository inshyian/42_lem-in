/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation_room.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/17 17:16:07 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:22:00 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void				validation_room(char **splitted, char *state, char *line)
{
	if (ft_strsplits_count(splitted) == 3)
	{
		if (ft_strchrcount(line, ' ') != 2)
			*state = -6;
		else if (splitted[0][0] == 'L' || ft_strchr(splitted[0], '-') ||
		splitted[0][0] == '#')
			*state = -4;
		else if (!ft_isnum(splitted[1]) || !ft_isnum(splitted[2]))
			*state = -5;
		else if (ft_longexprcmp(splitted[1], '>', "2147483647") ||
				ft_longexprcmp(splitted[2], '>', "2147483647"))
			*state = -19;
		else if (ft_longexprcmp(splitted[1], '<', "-2147483648") ||
				ft_longexprcmp(splitted[2], '<', "-2147483648"))
			*state = -18;
	}
	else
		*state = -6;
}
