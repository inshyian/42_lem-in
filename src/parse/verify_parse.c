/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verify_parse.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/30 17:58:13 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:28:18 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void		edge(char **splitted, t_ant_farm *farm, char *state, char *line)
{
	validation_edge(splitted, state, line);
	if (*state == 3)
		parse_edge(splitted, farm, state);
	ft_strsplits_free(&splitted);
}

void			room(char **splitted, t_ant_farm *farm, char *state, char *line)
{
	validation_room(splitted, state, line);
	if (*state == 2)
		parse_room(splitted, farm, state);
	else
		ft_strsplits_free(&splitted);
}

static void		count(char **splitted, t_ant_farm *farm, char *state,
							char *line)
{
	validation_ant_count(splitted, state, line);
	if (*state == 1)
		parse_ant_count(splitted, farm, state);
	ft_strsplits_free(&splitted);
}

void			verify_parse(char **splitted, t_ant_farm *farm, char *state,
							char *line)
{
	if (*state == 1)
	{
		count(splitted, farm, state, line);
		if (*state == 1)
			*state = 2;
	}
	else if (*state == 2 && ft_strsplits_count(splitted) == 1 && farm->rooms)
	{
		*state = 3;
		edge(ft_strsplit(splitted[0], '-'), farm, state, line);
		ft_strsplits_free(&splitted);
	}
	else if (*state == 2)
		room(splitted, farm, state, line);
	else if (*state == 3)
		edge(splitted, farm, state, line);
}
