/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_room.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 11:51:50 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/04 19:22:09 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void		add_room(t_room *new, t_ant_farm *farm)
{
	t_dllist	*item;

	item = ft_smalloc(sizeof(t_dllist));
	ft_bzero(item, sizeof(t_dllist));
	item->content = new;
	if (!farm->rooms)
	{
		farm->rooms = item;
		farm->last = item;
	}
	else
	{
		farm->last->next = item;
		item->prev = farm->last;
		farm->last = item;
	}
	farm->room_count++;
}

static void		check_room(char **splitted, t_dllist *rooms, char *state)
{
	while (rooms)
	{
		if (!ft_strcmp(((t_room *)rooms->content)->name, splitted[0]))
			*state = -12;
		else if (!ft_strcmp(((t_room *)rooms->content)->x, splitted[1]) &&
		!ft_strcmp(((t_room *)rooms->content)->y, splitted[2]))
			*state = -13;
		rooms = rooms->next;
	}
}

void			parse_room(char **splitted, t_ant_farm *farm, char *state)
{
	t_room		*new;

	check_room(splitted, farm->rooms, state);
	if (*state == 2)
	{
		new = ft_smalloc(sizeof(t_room));
		ft_bzero(new, sizeof(t_room));
		new->level = 2147483647;
		new->name = splitted[0];
		new->x = splitted[1];
		new->y = splitted[2];
		add_room(new, farm);
		ft_memdel((void **)&splitted);
	}
	else
		ft_strsplits_free(&splitted);
}
