/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_ant_count.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/29 11:51:50 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/07 22:01:23 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void			parse_ant_count(char **splitted, t_ant_farm *farm, char *state)
{
	farm->ant_count = ft_atoi(splitted[0]);
	(void)state;
}
