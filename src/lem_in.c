/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 17:40:22 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 14:01:09 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../inc/lem_in.h"

static void		save_flag_arg(int *i, int *argc, char **argv, t_flags *flags)
{
	(*i)++;
	if (*i < *argc)
	{
		if (ft_isdigit(argv[*i][0]) && ft_isnum(argv[*i]))
			flags->ant_num_to_show = ft_atoi(argv[*i]);
		else
			flags->error = 1;
	}
	else
		flags->error = 1;
}

static void		argv_cycle(int argc, char **argv, t_flags *flags)
{
	int			i;

	i = 1;
	while (i < argc && !flags->error)
	{
		if (!process_flag(argv[i], flags))
			break ;
		if (flags->show_ant_way && !flags->ant_num_to_show)
			save_flag_arg(&i, &argc, argv, flags);
		i++;
	}
}

int				main(int argc, char **argv)
{
	t_ant_farm	farm;

	ft_bzero(&farm, sizeof(t_ant_farm));
	argv_cycle(argc, argv, &farm.flags);
	if (farm.flags.print_help || farm.flags.error)
		usage();
	else
	{
		create_farm(&farm);
		if (!farm.error)
			analyze_graph(&farm);
		farm_free(&farm);
	}
	return (0);
}
