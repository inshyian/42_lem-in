/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_lines_of_file.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/24 21:31:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/07 13:56:00 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lem_in.h"

void		print_lines_of_file(t_list *lines)
{
	while (lines)
	{
		ft_printf("%s\n", lines->content);
		lines = lines->next;
	}
	ft_putchar('\n');
}
