/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   farm_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/30 20:31:48 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 10:11:29 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../inc/lem_in.h"

static void		free_ants(void *ptr, size_t content_size)
{
	free(ptr);
	(void)content_size;
}

static void		links_free(void *link, size_t content_size)
{
	(void)link;
	(void)content_size;
}

static void		rooms_free(void *room, size_t content_size)
{
	ft_lstdel(&((t_room *)room)->links, &links_free);
	free(((t_room *)room)->name);
	free(((t_room *)room)->x);
	free(((t_room *)room)->y);
	free(room);
	(void)content_size;
}

void			farm_free(t_ant_farm *farm)
{
	if (farm->ants)
		ft_dllstdel(&farm->ants, &free_ants);
	if (farm->ways)
		free_ways(farm);
	if (farm->rooms)
		ft_dllstdel(&farm->rooms, &rooms_free);
	if (farm->lines)
		ft_lstdel(&farm->lines, &ft_memsdel);
}
