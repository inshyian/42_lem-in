/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 12:41:50 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 17:28:42 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lem_in.h"

static void	example(void)
{
	ft_printf("\n\tValid data input example:\n\n");
	ft_printf("\t3\n");
	ft_printf("\t##start\n");
	ft_printf("\t1 11 21\n");
	ft_printf("\t##end\n");
	ft_printf("\t2 12 22\n");
	ft_printf("\t3 13 23\n");
	ft_printf("\t1-3\n");
	ft_printf("\t2-3\n");
}

static void	details(void)
{
	ft_printf("\tlem-in is an ant farm simulation. Ants must go from start \
room to end room along disjoint paths\n\n");
	ft_printf("\tlem-in accepts data from stdin\n\n");
	ft_printf("\tInput data format (3 blocks):\n");
	ft_printf("\t<ants>\n");
	ft_printf("\t<rooms>\n");
	ft_printf("\t<links>\n\n");
	ft_printf("\tFirst block contains single line with integer which \
represents count of ants\n");
	ft_printf("\tSecond block contains one room per line in format: \
<room_name> <x> <y>; <room_name> is an arbitrary name; <x> and <y> are \
integers\n");
	ft_printf("\tnote: second block must contain one line with \"##start\" \
command and one line with \"##end\" command, each followed by line with room. \
These rooms will be start and end respectively; \
<x> and <y> have no effect\n");
	ft_printf("\tThird block contains one link per line in format: \
<room_name>-<room_name>\n");
}

static void	flag_info(void)
{
	ft_printf("\tflag -h (--help) - display this usage\n");
	ft_printf("\tflag -e (--error-detalization) - display error details\n");
	ft_printf("\tflag -w (--print-ways) - display found ways\n");
	ft_printf("\tflag -a <ant_num> (--show-ant-way <ant_num>) \
- display certain ant's way\n\n");
}

static void	general(void)
{
	ft_printf("usage:\t");
	ft_printf("$>./lem-in [-h]\n");
	ft_printf("\t$>./lem-in [-ewa]\n");
	ft_printf("\t$>./lem-in [-ewa] < <source>\n\n");
}

void		usage(void)
{
	general();
	flag_info();
	details();
	example();
}
