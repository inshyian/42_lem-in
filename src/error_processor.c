/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_processor.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/19 14:36:25 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 17:16:42 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/lem_in.h"

static void	type_error__(char state)
{
	if (state == -30)
		ft_printf("No input");
	else if (state == -31)
		ft_printf("No start room");
	else if (state == -32)
		ft_printf("No end room");
	else if (state == -33)
		ft_printf("No rooms. No links");
	else if (state == -34)
		ft_printf("No links. End room is a priori unreachable");
	else if (state == -35)
		ft_printf("No ant count specified. No rooms. No links");
}

static void	type_error_(char state)
{
	if (state == -11)
		ft_printf("Double end room");
	else if (state == -12)
		ft_printf("Double room name");
	else if (state == -13)
		ft_printf("Double room coordinates");
	else if (state == -14)
		ft_printf("Commands ##start and ##end are appliable for rooms only");
	else if (state == -15)
		ft_printf("Link to itself");
	else if (state == -16)
		ft_printf("Nonexistent room in link");
	else if (state == -17)
		ft_printf("End room is unreachable");
	else if (state == -18)
		ft_printf("Too little coordinate value");
	else if (state == -19)
		ft_printf("Too big coordinate value");
	else
		type_error__(state);
}

static void	type_error(char state)
{
	if (state == -1)
		ft_printf("Wrong ant count");
	else if (state == -2)
		ft_printf("Too big ant count");
	else if (state == -3)
		ft_printf("Too little ant count");
	else if (state == -4)
		ft_printf("Wrong room name");
	else if (state == -5)
		ft_printf("Wrong room coordinates");
	else if (state == -6)
		ft_printf("Wrong room format");
	else if (state == -7)
		ft_printf("Wrong ant count format");
	else if (state == -8)
		ft_printf("Wrong link format");
	else if (state == -9)
		ft_printf("Wrong room name in link");
	else if (state == -10)
		ft_printf("Double start room");
	else
		type_error_(state);
}

void		error_processor(t_flags *flags, char state, int *line_num)
{
	if (!flags->error_detalization)
		ft_printf("ERROR\n");
	else
	{
		ft_printf("%{red}yERROR%{normal}y ");
		ft_printf("(%{bold}y%hd%{normal}y): ", state);
		if (line_num && *line_num != 0 && state > -30)
			ft_printf("line %d: ", *line_num);
		type_error(state);
		ft_putchar('\n');
	}
}
