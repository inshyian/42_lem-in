/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_flag.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/07 16:08:40 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 12:37:39 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "../../inc/lem_in.h"

static int		do_flag_long(char *flag, t_flags *flags)
{
	if (!ft_strcmp(flag, "--help"))
		flags->print_help = 1;
	else if (!ft_strcmp(flag, "--print-ways"))
		flags->print_ways = 1;
	else if (!ft_strcmp(flag, "--show-ant-way"))
		flags->show_ant_way = 1;
	else if (!ft_strcmp(flag, "--error-detalization"))
		flags->error_detalization = 1;
	else
	{
		flags->error = 1;
		return (0);
	}
	return (1);
}

static int		do_flag_short(char *flag, t_flags *flags)
{
	if (*flag == 'h')
		flags->print_help = 1;
	else if (*flag == 'w')
		flags->print_ways = 1;
	else if (*flag == 'a')
		flags->show_ant_way = 1;
	else if (*flag == 'e')
		flags->error_detalization = 1;
	else
	{
		flags->error = 1;
		return (0);
	}
	return (1);
}

static int		cycle_flags_short(char *flag, t_flags *flags)
{
	flag++;
	if (!*flag)
	{
		flags->error = 1;
		return (0);
	}
	while (*flag)
	{
		if (!do_flag_short(flag, flags))
			return (0);
		flag++;
	}
	return (1);
}

int				process_flag(char *flag, t_flags *flags)
{
	if (flag[0] == '-' && flag[1] != '-')
		return (cycle_flags_short(flag, flags));
	else if (flag[0] == '-' && flag[1] == '-')
		return (do_flag_long(flag, flags));
	return (0);
}
