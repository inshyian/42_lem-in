/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options_processor.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 21:08:06 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 16:50:49 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void		print_ant_way(t_ant_farm *farm)
{
	t_dllist	*track;

	ft_putchar('\n');
	track = farm->flags.ant_way;
	ft_printf("Ant %d (Way ID: %d): ", farm->flags.ant_num_to_show,
	((t_room *)track->content)->way_id);
	ft_printf("%s -> ", ((t_room *)farm->start->content)->name);
	while (track->next)
	{
		ft_printf("%s -> ", ((t_room *)track->content)->name);
		track = track->next;
	}
	ft_printf("%s", ((t_room *)farm->end->content)->name);
	ft_putchar('\n');
}

void			options_processor(t_ant_farm *farm)
{
	if (farm->flags.show_ant_way && farm->flags.ant_way)
		print_ant_way(farm);
	if (farm->flags.print_ways && farm->ways)
		print_ways(farm);
}
