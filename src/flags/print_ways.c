/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ways.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/05 11:52:42 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 16:30:48 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void		print_way(t_ant_farm *farm, t_dllist *way)
{
	t_dllist	*track;

	track = way->content;
	ft_printf("Way ID %d (Edges: %zu): %s -> ",
	((t_room *)track->content)->way_id,
	(way->content_size),
	((t_room *)farm->start->content)->name);
	while (track->next)
	{
		ft_printf("%s -> ", ((t_room *)track->content)->name);
		track = track->next;
	}
	ft_printf("%s", ((t_room *)farm->end->content)->name);
}

void			print_ways(t_ant_farm *farm)
{
	t_dllist		*ways;

	ft_putchar('\n');
	ways = farm->ways;
	while (ways)
	{
		print_way(farm, ways);
		ft_putchar('\n');
		ways = ways->next;
	}
}
