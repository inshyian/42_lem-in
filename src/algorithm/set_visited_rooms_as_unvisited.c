/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_visited_rooms_as_unvisited.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 09:55:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/06 20:15:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void	set_room_room_visited_to_zero(t_dllist *rooms)
{
	while (rooms)
	{
		((t_room *)rooms->content)->level = 2147483647;
		((t_room *)rooms->content)->visited = 0;
		rooms = rooms->next;
	}
}

void		set_visited_rooms_as_unvisited(t_ant_farm *farm)
{
	set_room_room_visited_to_zero(farm->rooms);
}
