/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_link_from_queue.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/31 18:02:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/08/30 09:30:13 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

/*
** Queue form:
** {queue}(t_queue)->
** {add_here;get_here(queue_item)}(t_dllist)->
** {content(link)}(t_list)->
** {content(room)}(t_room)
*/

t_list		*get_link_from_queue(t_queue *queue)
{
	t_dllist	*queue_item;
	t_list		*link;

	queue_item = queue->get_here;
	link = queue_item->content;
	if (queue_item->prev)
		queue->get_here = queue_item->prev;
	else
	{
		queue->get_here = NULL;
		queue->add_here = NULL;
	}
	ft_memdel((void **)&queue_item);
	link->content_size = 1;
	return (link);
}
