/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_link_to_queue.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/31 18:02:36 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/02 15:29:11 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

/*
** Queue form:
** {queue}(t_queue)->
** {add_here;get_here(queue_item)}(t_dllist)->
** {content(link)}(t_list)->
** {content(room)}(t_room)
*/

static t_dllist		*create_queue_item(t_list *link)
{
	t_dllist		*queue_item;

	queue_item = ft_smalloc(sizeof(t_dllist));
	ft_bzero(queue_item, sizeof(t_dllist));
	queue_item->content = link;
	return (queue_item);
}

void				add_link_to_queue(t_queue *queue, t_list *link)
{
	t_dllist		*new_queue_item;

	new_queue_item = create_queue_item(link);
	ft_dllstadd(&queue->add_here, new_queue_item);
	if (!queue->get_here)
		queue->get_here = queue->add_here;
}
