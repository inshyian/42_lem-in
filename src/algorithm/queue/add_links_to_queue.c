/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_links_to_queue.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 15:13:47 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/06 20:32:57 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

/*
** Queue form:
** {queue}(t_queue)->
** {add_here;get_here(queue_item)}(t_dllist)->
** {content(link)}(t_list)->
** {content(room)}(t_room)
*/

void			add_links_to_queue(t_queue *queue, t_list *link)
{
	while (link)
	{
		if (!((t_room *)link->content)->visited &&
		!link->content_size &&
		!((t_room *)link->content)->way_id)
			add_link_to_queue(queue, link);
		link = link->next;
	}
}
