/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_ways_length.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 11:55:29 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/05 11:58:59 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "../../../inc/lem_in.h"

void			calc_ways_length(t_ant_farm *farm)
{
	t_dllist	*ways;

	farm->the_shortest_way_len = INT_MAX;
	ways = farm->ways;
	while (ways)
	{
		ways->content_size = ft_dllstlen(ways->content, NULL);
		if (farm->the_shortest_way_len > (int)ways->content_size)
			farm->the_shortest_way_len = ways->content_size;
		ways = ways->next;
	}
}
