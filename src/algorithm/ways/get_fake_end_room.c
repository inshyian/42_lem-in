/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_fake_end_room.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 14:00:11 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/03 14:39:53 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

t_room		*get_fake_end_room(t_ant_farm *farm, int way_id)
{
	t_room	*fake_end_room;

	fake_end_room = ft_memdup(farm->end->content, sizeof(t_room));
	fake_end_room->way_id = way_id;
	return (fake_end_room);
}
