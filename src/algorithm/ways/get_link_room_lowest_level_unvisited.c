/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_link_room_lowest_level_unvisited.c             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 11:00:59 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/06 20:32:12 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static int	compare(t_list *lowest_level_unvisited, t_list *curr_link)
{
	if (((t_room *)curr_link->content)->way_id == 0)
	{
		if (!lowest_level_unvisited)
			return (1);
		else if (((t_room *)lowest_level_unvisited->content)->level >
		((t_room *)curr_link->content)->level)
			return (1);
	}
	return (0);
}

t_list		*get_link_lowest_level_unvisited(t_list *links)
{
	t_list	*lowest_level_unvisited;

	lowest_level_unvisited = NULL;
	while (links)
	{
		if (links->content_size == 0)
			if (compare(lowest_level_unvisited, links))
				lowest_level_unvisited = links;
		links = links->next;
	}
	if (lowest_level_unvisited)
		lowest_level_unvisited->content_size = 1;
	return (lowest_level_unvisited);
}
