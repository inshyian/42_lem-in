/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_ways.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 21:41:51 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/03 22:05:00 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static int		sort_func(t_dllist *a, t_dllist *b)
{
	if (a->content_size < b->content_size)
		return (1);
	return (0);
}

void			sort_ways(t_ant_farm *farm)
{
	ft_quicksort(&farm->ways, NULL, sort_func);
}
