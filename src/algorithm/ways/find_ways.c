/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_ways.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 10:07:53 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 13:56:21 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void	add_room_to_way(t_dllist **way, t_room *room)
{
	t_dllist	*way_new;

	way_new = ft_smalloc(sizeof(t_dllist));
	ft_bzero(way_new, sizeof(t_dllist));
	way_new->content = room;
	((t_room *)way_new->content)->way_id = ((t_room *)(*way)->content)->way_id;
	ft_dllstadd(way, way_new);
}

static void	remove_room_from_way(t_dllist **way)
{
	t_dllist	*way_next;

	((t_room *)(*way)->content)->way_id = 0;
	way_next = (*way)->next;
	free(*way);
	*way = way_next;
}

static int	builder_recursive(t_dllist **way)
{
	t_list		*curr_way_room_link;

	if (((t_room *)(*way)->content)->level == 1)
		return (1);
	curr_way_room_link =
	get_link_lowest_level_unvisited(((t_room *)(*way)->content)->links);
	while (curr_way_room_link)
	{
		add_room_to_way(way, curr_way_room_link->content);
		if (builder_recursive(way))
			return (1);
		remove_room_from_way(way);
		curr_way_room_link =
		get_link_lowest_level_unvisited(((t_room *)(*way)->content)->links);
	}
	return (0);
}

static void	build_way(int way_id, t_ant_farm *farm, t_list *curr_end_room_link)
{
	t_dllist	*way;

	way = ft_smalloc(sizeof(t_dllist));
	ft_bzero(way, sizeof(t_dllist));
	way->content = get_fake_end_room(farm, way_id);
	((t_room *)way->content)->way_id = way_id;
	add_room_to_way(&way, curr_end_room_link->content);
	if (builder_recursive(&way))
	{
		ft_dllstadd(&farm->ways, ft_dllstnew(way, sizeof(t_dllist)));
		free(way);
		set_rooms_links_as_unused(farm);
		set_visited_rooms_as_unvisited(farm);
		set_rooms_depth_dijkstra(farm);
	}
	else
		free_way(way);
}

void		find_ways(t_ant_farm *farm)
{
	t_list		*end_room_links;
	t_list		*curr_end_room_link;
	int			way_id;

	way_id = 1;
	end_room_links = ((t_room *)farm->end->content)->links;
	((t_room *)farm->end->content)->way_id = -1;
	curr_end_room_link = get_link_lowest_level_unvisited(end_room_links);
	while (curr_end_room_link)
	{
		build_way(way_id, farm, curr_end_room_link);
		way_id++;
		curr_end_room_link = get_link_lowest_level_unvisited(end_room_links);
	}
}
