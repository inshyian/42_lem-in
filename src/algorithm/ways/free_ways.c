/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_ways.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 15:46:31 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 10:47:08 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void		free_begins_of_ways(t_ant_farm *farm, t_dllist *ways)
{
	t_dllist	*tofree;

	while (ways)
	{
		tofree = ways;
		ways = ways->next;
		free(tofree);
	}
	farm->ways = NULL;
}

void			free_way(t_dllist *way)
{
	t_dllist	*tofree;

	while (way->next)
	{
		tofree = way;
		way = way->next;
		free(tofree);
	}
	free(way->content);
	free(way);
}

static void		free_ways_(t_dllist *ways)
{
	while (ways)
	{
		free_way(ways->content);
		ways->content = NULL;
		ways = ways->next;
	}
}

void			free_ways(t_ant_farm *farm)
{
	free_ways_(farm->ways);
	free_begins_of_ways(farm, farm->ways);
}
