/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_rooms_links_as_unused.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/30 09:55:41 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/06 17:13:37 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void	set_room_links_unused(t_room *room)
{
	t_list	*links;

	links = room->links;
	while (links)
	{
		links->content_size = 0;
		links = links->next;
	}
}

static void	goer_thought_rooms(t_dllist *rooms)
{
	while (rooms)
	{
		set_room_links_unused(rooms->content);
		rooms = rooms->next;
	}
}

void		set_rooms_links_as_unused(t_ant_farm *farm)
{
	goer_thought_rooms(farm->rooms);
}
