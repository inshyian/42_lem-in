/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analyze_graph.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/31 17:45:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 09:50:25 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static int		is_exist_start_end_link(t_ant_farm *farm)
{
	t_list		*start_links;

	start_links = ((t_room *)farm->start->content)->links;
	while (start_links)
	{
		if (((t_room *)start_links->content)->name ==
		((t_room *)farm->end->content)->name)
			return (1);
		start_links = start_links->next;
	}
	return (0);
}

void			analyze_graph(t_ant_farm *farm)
{
	if (!is_exist_start_end_link(farm))
	{
		set_visited_rooms_as_unvisited(farm);
		set_rooms_links_as_unused(farm);
		set_rooms_depth_dijkstra(farm);
		find_ways(farm);
		if (!farm->ways)
		{
			error_processor(&farm->flags, -17, 0);
			return ;
		}
		print_lines_of_file(farm->lines);
		calc_ways_length(farm);
		sort_ways(farm);
		ants_on_the_way(farm);
	}
	else
	{
		print_lines_of_file(farm->lines);
		print_ants_in_end_in_one_way(farm);
	}
	options_processor(farm);
}
