/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_rooms_depth_dijkstra.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/05 11:05:04 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/06 20:34:40 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

static void	set_depth_near_curr(t_room *room)
{
	t_list	*links;

	links = room->links;
	while (links)
	{
		if ((room->level + 1) < ((t_room *)links->content)->level)
		{
			if (!((t_room *)links->content)->way_id)
				((t_room *)links->content)->level = room->level + 1;
		}
		links = links->next;
	}
}

t_list		*set_rooms_depth_dijkstra(t_ant_farm *farm)
{
	t_list		*link;
	t_queue		queue;

	((t_room *)farm->start->content)->visited = 1;
	((t_room *)farm->start->content)->level = 0;
	set_depth_near_curr((t_room *)farm->start->content);
	ft_bzero(&queue, sizeof(t_queue));
	add_links_to_queue(&queue, ((t_room *)farm->start->content)->links);
	while (queue.get_here)
	{
		link = get_link_from_queue(&queue);
		set_depth_near_curr(link->content);
		add_links_to_queue(&queue, ((t_room *)link->content)->links);
		((t_room *)link->content)->visited = 1;
	}
	return (NULL);
}
