/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ants_in_end_in_one_way.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 17:16:01 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/09 13:13:46 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../inc/lem_in.h"

void		print_ants_in_end_in_one_way(t_ant_farm *farm)
{
	int		curr_ant;

	curr_ant = 1;
	while (curr_ant <= farm->ant_count)
	{
		if (curr_ant == farm->flags.ant_num_to_show)
			ft_printf("%{bold,yellow}yL%d-%s%{normal}y", curr_ant,
			((t_room *)farm->end->content)->name);
		else
			ft_printf("L%d-%s", curr_ant,
			((t_room *)farm->end->content)->name);
		if (curr_ant != farm->ant_count)
			ft_putchar(' ');
		curr_ant++;
	}
	ft_putchar('\n');
}
