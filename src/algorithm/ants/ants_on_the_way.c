/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants_on_the_way.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 10:43:02 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/07 21:49:24 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void	launch_ants(t_ant_farm *farm)
{
	while (!does_all_ants_finished(farm))
	{
		put_ants_in_rooms(farm);
		print_ants_on_the_way(farm);
		one_move(farm);
	}
}

void		ants_on_the_way(t_ant_farm *farm)
{
	create_ant_list(farm);
	launch_ants(farm);
}
