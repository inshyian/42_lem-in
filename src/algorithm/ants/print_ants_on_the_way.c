/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ants_on_the_way.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/05 11:52:42 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/07 22:08:50 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void		print_ant(t_ant_farm *farm, t_ant *ant)
{
	if (ant->ant_num == farm->flags.ant_num_to_show)
		ft_printf("%{bold,yellow}yL%d-%s%{normal}y", ant->ant_num,
		((t_room *)((t_dllist *)ant->curr_way_item)->content)->name);
	else
		ft_printf("L%d-%s", ant->ant_num,
		((t_room *)((t_dllist *)ant->curr_way_item)->content)->name);
}

void			print_ants_on_the_way(t_ant_farm *farm)
{
	t_dllist		*ants;
	unsigned char	trigger;

	ants = farm->ants;
	trigger = 0;
	while (ants)
	{
		if (((t_ant *)ants->content)->curr_way_item)
		{
			if (trigger)
				ft_putchar(' ');
			print_ant(farm, ants->content);
			trigger = 1;
		}
		ants = ants->next;
	}
	ft_putchar('\n');
}
