/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_ant_list.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 15:07:33 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/08 10:11:31 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void	add_ant_to_list(t_ant_farm *farm, int ant_num)
{
	t_ant	*ant;

	ant = ft_memalloc(sizeof(t_ant));
	ant->finished = 0;
	ant->ant_num = ant_num;
	ft_dllstadd(&farm->ants, ft_dllstnew(ant, sizeof(t_ant)));
	free(ant);
}

void		create_ant_list(t_ant_farm *farm)
{
	int		ant_num;

	ant_num = farm->ant_count;
	while (ant_num >= 1)
	{
		add_ant_to_list(farm, ant_num);
		ant_num--;
	}
}
