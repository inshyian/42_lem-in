/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   does_all_ants_finished.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 18:57:43 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/03 19:03:44 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

int		does_all_ants_finished(t_ant_farm *farm)
{
	t_dllist	*ants;

	ants = farm->ants;
	while (ants)
	{
		if (!((t_ant *)ants->content)->finished)
			return (0);
		ants = ants->next;
	}
	return (1);
}
