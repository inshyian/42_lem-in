/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_ants_in_rooms.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 21:32:40 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/07 21:53:41 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

int				should_ant_go_this_way(t_ant_farm *farm, t_dllist *way)
{
	t_dllist	*ways;
	int			sum;

	ways = farm->ways;
	sum = 0;
	while (ways != way)
	{
		sum = sum + (way->content_size - ways->content_size);
		ways = ways->next;
	}
	if (farm->ant_count >= sum)
		return (1);
	return (0);
}

t_dllist		*get_the_shortest_free_way(t_dllist *ways)
{
	t_dllist	*the_shortest_free_way;

	the_shortest_free_way = NULL;
	while (ways)
	{
		if (ways->content)
			if (((t_room *)((t_dllist *)ways->content)->content)->occupied == 0)
			{
				if (!the_shortest_free_way)
					the_shortest_free_way = ways;
				else if (ways->content_size <
				the_shortest_free_way->content_size)
					the_shortest_free_way = ways;
			}
		ways = ways->next;
	}
	if (the_shortest_free_way)
		return (the_shortest_free_way);
	return (NULL);
}

t_dllist		*find_suitable_way(t_ant_farm *farm)
{
	t_dllist	*the_shortest_free_way;

	the_shortest_free_way = get_the_shortest_free_way(farm->ways);
	if (!the_shortest_free_way)
		return (NULL);
	else if (!should_ant_go_this_way(farm, the_shortest_free_way))
		return (NULL);
	farm->ant_count--;
	return (the_shortest_free_way->content);
}

void			put_ants_in_rooms(t_ant_farm *farm)
{
	t_dllist	*suitable_way_first_item;
	t_dllist	*ants;

	ants = farm->ants;
	suitable_way_first_item = find_suitable_way(farm);
	while (suitable_way_first_item && ants)
	{
		if (!((t_ant *)ants->content)->finished &&
		!((t_ant *)ants->content)->curr_way_item)
		{
			((t_room *)suitable_way_first_item->content)->occupied = 1;
			((t_ant *)ants->content)->curr_way_item = suitable_way_first_item;
			if (((t_ant *)ants->content)->ant_num ==
			farm->flags.ant_num_to_show)
				farm->flags.ant_way = suitable_way_first_item;
			suitable_way_first_item = find_suitable_way(farm);
			ants = ants->next;
		}
		else
			ants = ants->next;
	}
}
