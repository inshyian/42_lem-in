/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   one_move.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 11:10:54 by ishyian           #+#    #+#             */
/*   Updated: 2019/09/03 18:54:35 by ishyian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../inc/lem_in.h"

static void		free_first_rooms_from_ants(t_dllist *ways)
{
	while (ways)
	{
		((t_room *)((t_dllist *)ways->content)->content)->occupied = 0;
		ways = ways->next;
	}
}

static void		single_ant_move(t_ant *ant)
{
	ant->curr_way_item = ant->curr_way_item->next;
	if (ant->curr_way_item == NULL)
		ant->finished = 1;
}

void			one_move(t_ant_farm *farm)
{
	t_dllist	*ants;

	free_first_rooms_from_ants(farm->ways);
	ants = farm->ants;
	while (ants)
	{
		if (((t_ant *)ants->content)->curr_way_item)
			single_ant_move(ants->content);
		ants = ants->next;
	}
}
