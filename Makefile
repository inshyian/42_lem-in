# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ishyian <ishyian@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/30 14:57:31 by ishyian           #+#    #+#              #
#    Updated: 2019/09/09 16:13:48 by ishyian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC =				gcc
NAME	 =			lem-in
CFLAGS =			-Wall -Wextra -Werror
SRCS =				./src/lem_in.c\
					./src/print_lines_of_file.c\
					./src/usage.c\
					./src/parse/create_farm.c\
					./src/parse/save_line.c\
					./src/parse/verify_parse.c\
					./src/parse/command.c\
					./src/parse/parse_ant_count.c\
					./src/parse/parse_edge.c\
					./src/parse/parse_room.c\
					./src/parse/validation_ant_count.c\
					./src/parse/validation_edge.c\
					./src/parse/validation_room.c\
					./src/algorithm/analyze_graph.c\
					./src/algorithm/set_rooms_depth_dijkstra.c\
					./src/algorithm/set_visited_rooms_as_unvisited.c\
					./src/algorithm/set_rooms_links_as_unused.c\
					./src/algorithm/print_ants_in_end_in_one_way.c\
					./src/algorithm/queue/add_link_to_queue.c\
					./src/algorithm/queue/add_links_to_queue.c\
					./src/algorithm/queue/get_link_from_queue.c\
					./src/algorithm/ways/find_ways.c\
					./src/algorithm/ways/get_link_room_lowest_level_unvisited.c\
					./src/algorithm/ways/free_ways.c\
					./src/algorithm/ways/get_fake_end_room.c\
					./src/algorithm/ways/sort_ways.c\
					./src/algorithm/ways/calc_ways_length.c\
					./src/algorithm/ants/ants_on_the_way.c\
					./src/algorithm/ants/one_move.c\
					./src/algorithm/ants/put_ants_in_rooms.c\
					./src/algorithm/ants/print_ants_on_the_way.c\
					./src/algorithm/ants/create_ant_list.c\
					./src/algorithm/ants/does_all_ants_finished.c\
					./src/flags/options_processor.c\
					./src/flags/print_ways.c\
					./src/flags/process_flag.c\
					./src/error_processor.c\
					./src/farm_free.c

OBJ =				$(SRCS:.c=.o)
LIBS = 				-lft -lncurses -lmlx -framework OpenGL -framework Appkit
LIB_DIR =			./libft/
LIB_FT =			./libft/libft.a
INCLUDES =			./inc/lem_in.h

all: 				$(NAME)

$(NAME):			$(LIB_FT) $(OBJ)
					$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIBS) -L $(LIB_DIR)

$(OBJ): 			%.o: %.c $(INCLUDES)
					$(CC) $(CFLAGS) -I $(INCLUDES) -o $@ -c $<

$(LIB_FT):			libft
					@make -C $(LIB_DIR)

libft:
					@make -C $(LIB_DIR)

clean:
					rm -f $(OBJ)
					make clean -C $(LIB_DIR)

fclean: 			clean
					rm -f $(NAME)
					make fclean -C $(LIB_DIR)

re: 				fclean all

.PHONY: 			all libft clean fclean
